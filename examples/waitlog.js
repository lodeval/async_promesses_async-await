const waitLogResolve = (n, message) => 
  new Promise( (resolve, reject) => {
    setTimeout(() => resolve(message), n*1000)
  }) // always resolve 

const waitLogReject = (n, message) => 
  new Promise( (resolve, reject) => {
    setTimeout(() => reject(message), n*1000)
  }) // always reject

let p = waitLogResolve(2, 'resolved !')
let r = waitLogReject(2, 'such a loser !')


p.then(res => console.log(res), null) // p always resolves
r.then(null, err => console.error(err)) // r always rejects
console.log('first')
// first
// resolved !
// such a loser !
