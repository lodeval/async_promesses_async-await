function ps(p){
  p.status = 'pending'
  return p.then(
    r => {
      p.status = 'fullfilled'
      return r
    }, 
    r => { 
      p.status = 'rejected'
      return r
    }
  )
}
