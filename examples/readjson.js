const readJSON = (fileName, cb) => 
  fs.readFile(fileName, 'utf8', (err, res) => {
    if (err) return cb(err)
    try {
      cb(null, JSON.parse(res))
    } catch (e) {
      cb(e)
    }
  })

const readJSONprom = (fileName) => new Promise((resolve, reject) => {
  fs.readFile(fileName, 'utf8', (err, res) => {
    if (err) return reject(err)
    return resolve(JSON.parse(res))
  })
})
