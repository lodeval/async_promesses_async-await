const fs = require('fs')
const path = require('path')
const inspect = require('util').inspect
const properties = ['name', 'energy', 'fat', 'saturated', 'carbs', 'sugar', 'salt']
module.exports = function(directory){
  function toFileSync(f){
    const outputFile = path.join(directory, f.name + '.json')
    fs.writeFileSync(outputFile, JSON.stringify(f))
  }

  function fromFileSync(name){
    const inFile = path.join(directory, name + '.json')
    return JSON.parse(fs.readFileSync(inFile,'utf8'))
  }

  function toFile(f, cb) {
    const outputFile = path.join(directory, f.name.replace(' ','_') + '.json')
    fs.writeFile(outputFile, JSON.stringify(f), cb)
  }

  function fromFile(name,cb){
    const inFile = path.join(directory, name.replace(' ','_') + '.json')
    fs.readFile(inFile, 'utf8', (err, data) => {
      if (err) return cb(err); // file does not exist (1)
      try {
        const food = JSON.parse(data)
        return cb(null, food) // return result ! (2)
      } catch (e){ // JSON.parse failed (3)
        cb(new Error(inFile, ' wrong format, json parse error: ',e))
      }
    })
  }


  function fromFileProm(name){
    return new Promise((resolve, reject)=> {
      const inFile = path.join(directory, name.replace(' ','_') + '.json')
      fs.readFile(inFile, 'utf8', (err, data) => {
        if (err) return reject(err); // file does not exist (1)
        try {
          const food = JSON.parse(data)
          return resolve(food) // return result ! (2)
        } catch (e){ // JSON.parse failed (3)
          reject(new Error(inFile, ' wrong format, json parse error: ',e))
        }
      })
    })
  }

  function allFromFileProm() {
    return new Promise((resolve, reject)=> {
      
    })
    
  }
  function allFromFile(cb){
    fs.readdir(directory, (err, files) => {
      if (err) return cb(err)
      const allNames = files.filter(f => f.match(/.json$/)).map(f => f.replace(/\.json$/,''))
      // read each file async, then do stg
      let foods = []
      allNames.forEach(f => fromFile(f, (err, res) =>{
        
        if (err) {
          foods = null
          return cb(err)
        }
        if (foods){
          foods.push(res)
          if (foods.length === allNames.length)
            return cb(null,foods)
        }
      }))
    })
  }
  return {toFile, fromFile, toFileSync, fromFileSync, allFromFile, properties, fromFileProm}
}
