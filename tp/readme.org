* Promisify food_module

- étudiez le code fourni, faites le tourner.
- créer =fromFileProm= et =allFromFileProm=, les versions promissifiées de
  ~fromFile~ et ~allFromFile~ 
- version bilingue : les fonctions d'origine doivent maintenant pouvoir être
  appelées indifféremment avec ou sans callback en argument.
- à tester dans node, examiner en particulier la valeur de la promesse.

** async/await
Essayez d'utilisez async/await au lieu des chaînes de promesses

* ls -l | sort ?

** Exercice préparatoire

Écrivez une fonction renvoyant sous forme de promesse la taille du fichier dont
le chemin est passé en argument.

Pensez à gérer les cas d'erreur.

Vous utiliserez =fs.stat()=, regardez donc la doc.

** Promesses en //

Écrivez une fonction renvoyant le contenu d'un répertoire, trié par ordre de
taille de fichier décroissante.

À utiliser:

- =fs.readdir=
- =fs.stat=
- =Promise.all=
- la fonction précédente ?  

** Bonus (dur !)

Implémenter la même chose en utilisant uniquement des callbacks.

