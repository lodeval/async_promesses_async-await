#+Title: Promesses
#+Author: Pierre Gambarotto
#+Email: pierre.gambarotto@enseeiht.fr
#+OPTIONS: num:nil reveal_title_slide:auto toc:nil
#+OPTIONS: reveal_rolling_links:t reveal_center:nil
#+REVEAL_PLUGINS: (markdown notes zoom)

#+REVEAL_MARGIN: 0.01
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_THEME: sky
#+REVEAL_EXTRA_CSS: ./local.css
#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/

# C-c C-e v b|v to export as a reveal js presentation

* Programmation asynchrone events/callback

- callback : très éloigné de la programmation synchrone
  - traitement du résultat par la fonction callback
  - traitement d'erreur itou
- contrôle du flux d'exécution difficile et peu lisible

#+REVEAL: split
#+BEGIN_SRC javascript
function funcSync(arg) {
  // do stuff with arg
  return something
}
let res = funcSync(arg) // synchronous call

// define async function
function funcAsync(arg, callback){
  // call existing async stuff
  existing_async_stuff(…, (err, result) => {
   … callback(err) … // handle err
   … callback(null, result) … // handle result
  })
}

// asynchronous call
funcAsync(a_val, (err, res) => {
  if (err) return handle(err)
  do_something_with(res)
})
#+END_SRC

** Exemple

#+BEGIN_SRC javascript :tangle /tmp/prom.js
const fs = require('fs')

const readJSONsync = (fileName) => 
  JSON.parse(fs.readFileSync(fileName, 'utf8'))

let res = readJSONsync('/tmp/test.json')
console.log(res)

const readJSON = (fileName, cb) =>
  fs.readFile(fileName, 'utf8', (err, res) => {
    if (err) return cb(err)
    cb(null, JSON.parse(res))
  })

readJSON('/tmp/test', (err, res) => {
  if (err) return console.log(err)
  console.log(res)
})
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC javascript
// error handling !!!
const fs = require('fs')

// synchronous: error handling outside function call
const readJSONsync = (fileName) => 
  JSON.parse(fs.readFileSync(fileName, 'utf8')
try {
  let o = readJSONsync('/tmp/foo.json')
} catch (e) { // file not found OR parse error
  console.error(e)
}

// asynchronous: error handling inside function body :-(
const readJSON = (fileName, cb) => 
  fs.readFile(fileName, 'utf8', (err, res) => {
    if (err) return cb(err) // file not found
    try {
      cb(null, JSON.parse(res))
    } catch (e) {
      cb(e) // parse error
    }
  })
#+END_SRC


** Chaînage d'opération


#+BEGIN_SRC javascript
// synchronous version
let res= funcSync(a_val)
let res2 = funcSync(res)
let res3 = funcSync(res2)
#+END_SRC

imbrication des callbacks: peu lisible :-(

#+BEGIN_SRC javascript
// pyramid of doom
funcAsync(a_val, (err, res) => {
  if (err) return err
  funcAsync2(res, (err2, res2) => {
    if (err2) return err2
    funcAsync3(res2, (err3, res3) => {
      if (err3) return err3
      return do_stuff(res3)
    })
  })
})
#+END_SRC

#+REVEAL: split
- faire N opération, puis …
#+BEGIN_SRC javascript
// synchronous version
const stuff2do = [ f1, f2, f3 ]
const res = stuff2do.map(f => f()) // f1() then f2() then f3()
exploitResults(res)
#+END_SRC

#+BEGIN_SRC javascript
// asynchronous Rendez-vous
const asyncStuff2do = [ f1, f2, f3] // fX returns result with a callback
let res = []
asyncStuff2do.forEach( f => // [ f1() // f2() // f3() ] !!
  f(resF => {
    res.push(resF)
    // detect if it was the last one
    if (res.length === asyncStuff2do.length)
      exploitResults(res)
  })
)
#+END_SRC

Exemple: [[https://gitlab.com/fullstack2018/food_module/blob/99e63fb46d8ef690bef4e80cdcbcc91cb96dff29/index.js#L34-53][food.allFromFile(cb)]]

* Promesse : concept

- [[https://caniuse.com/#feat=promises][ES6, disponible partout]]
- très utilisé côté API navigateur
- écrire du code asynchrone
- qui ressemble à du code synchrone

*Promise* encapsule le résultat à venir d'un appel asynchrone

*.then()* permet de récupérer le résultat d'une promesse quand il sera disponible

- soit une valeur : _fullfillment value_
- soit une erreur : _rejection reason_


#+REVEAL: split
#+BEGIN_SRC javascript
// returns a value of Promise type 
let promise = doSomethingAync() 
promise.then(onFulfilled, onRejected)
// onFulfilled, onRejected
// called when asynchronous task has a result
#+END_SRC

*Valeur possibles d'une promesse*
1. *Pending* opération asynchrone en cours
2. *Fullfilled* résultat disponible
3. *Rejected* raison d'erreur disponible

Cet état interne n'est pas exposé.

#+REVEAL: split

#+BEGIN_SRC plantuml :file i/promise_state.png
@startuml
[*] --> Pending : new Promise(…)
Pending: after creation
Pending -> Resolution : wait for async op.
state Resolution {
[*] -> Fullfilled
Fullfilled: async op. is a success
Fullfilled: result is available
[*] -> Rejected
Rejected: async op. is a failure
Rejected: error is available
}
@enduml
#+END_SRC

#+RESULTS:
[[file:i/promise_state.png]]

** Séparer la définition d'un traitement de l'exploitation des résultats
#+begin_src javascript
nodejsCallbackStyle(args, (err, res) => {
  if (err) return callback_error(err)
  callback_ok(res)
}

const prom = promiseEquivalent(args)
prom.then(callback_ok,callback_error)
#+end_src


#+reveal: split

#+begin_src javascript
fs.readdir('/tmp/', (err, files) => {
  if (err) return callback_error(err)
  callback_ok(files)
}

fs.readdirProm('/tmp'/).then(callback_ok,callback_error)
#+end_src

** Créer une promesse
constructeur ~Promise~: on lui passe une fonction à 2 arguments:
- le premier indique que faire en cas de succès
- le 2e indique que faire en cas d'erreur

#+begin_src javascript 
new Promise( (resolve, reject) => {
  // do stuff
  // if success, call resolve callback with result
  return resolve(res)
  // if err, call reject callback with err
  return reject(err)

}
#+end_src

#+reveal: split

[[file:examples/basic.js][examples/basic.js]]
#+begin_src javascript :tangle examples/basic.js 
// without promises
const oldstyle = (successCb, errorCb) => {
  console.log('stuff done')
  return (Math.random() > .5) ? successCb('yes') : errorCb('nope')
}

// call AND exploit result at the same time
oldstyle(msg => console.log(msg), err => console.error(err))

// with promises
const newstyle = () => new Promise( (resolve, reject) => {
  console.log('stuff done')
  return (Math.random() > .5) ? resolve('yes') : reject('nope')
})

// call then exploit results, 2 separate steps
const prom = newstyle() 
prom.then(msg => console.log(msg), err => console.log(err))
#+end_src

** De callback à promesse

#+BEGIN_SRC javascript
fs.readdir('/tmp', (err, files) => {
  if (err) return console.err(err)
  console.log(files)
})

function readdirPromise(path){
  return new Promise(function(resolve, reject){
    fs.readdir(path, function(err, files){
      if (err) return reject(err)
      resolve(files)
    })
  })
}

let promise = readdirPromise('/tmp')
promise.then(console.log, console.error)
#+END_SRC

- *valeur de la promesse* état de l'opération asynchrone
- *then* «ouvrir» la promesse pour exploiter le résultat
- arguments de *then* : fonction pour exploiter le résultat

** Exemple

[[file:examples/waitlog.js][examples/waitlog.js]]
#+BEGIN_SRC javascript :tangle examples/waitlog.js
const waitLogResolve = (n, message) => 
  new Promise( (resolve, reject) => {
    setTimeout(() => resolve(message), n*1000)
  }) // always resolve 

const waitLogReject = (n, message) => 
  new Promise( (resolve, reject) => {
    setTimeout(() => reject(message), n*1000)
  }) // always reject
#+END_SRC
** Then

#+BEGIN_SRC javascript
let promise = async_stuff(args)
promise.then(onFullfilled, onRejected)
#+END_SRC
- *then* est une fonction asynchrone
- *Pending* -> *Fullfilled* : *onFullfilled* est appelée, avec le résultat
- *Pending* -> *Rejected* : *onRejected* est appelée, avec l'erreur
- Attention: les callbacks ne prennent qu'un argument !

** Exemple


[[file:examples/waitlog.js][examples/waitlog.js]]
#+BEGIN_SRC javascript :tangle examples/waitlog.js
let p = waitLogResolve(2, 'resolved !')
let r = waitLogReject(2, 'such a loser !')


p.then(res => console.log(res), null) // p always resolves
r.then(null, err => console.error(err)) // r always rejects
console.log('first')
// first
// resolved !
// such a loser !
#+END_SRC

** Exemple

[[file:examples/readjson.js][examples/readjson.js]]
#+BEGIN_SRC javascript :tangle examples/readjson.js
const readJSON = (fileName, cb) => 
  fs.readFile(fileName, 'utf8', (err, res) => {
    if (err) return cb(err)
    try {
      cb(null, JSON.parse(res))
    } catch (e) {
      cb(e)
    }
  })

const readJSONprom = (fileName) => new Promise((resolve, reject) => {
  fs.readFile(fileName, 'utf8', (err, res) => {
    if (err) return reject(err)
    return resolve(JSON.parse(res))
  })
})
#+END_SRC

#+reveal: split

#+BEGIN_SRC javascript
// node command to read a file
.load examples/readjson.js
// 
let p = readJSONprom('/tmp/test.json')
p.then(res => console.log(res))
p.then(console.log) // the same thing !
let bad = readJSONprom("I don't exist")
p.then(null, err => console.error)
p.then(null, console.error)
#+END_SRC
** Then again

- *then* renvoie une promesse !
- pyramid of doom : NIET !
- ~op1.then(op2).then(op3)~
- idem pipe unix ~op1 | op2 | op3~

#+BEGIN_SRC javascript
// pyramid of doom
funcAsync(a_val, (err, res) => {
  if (err) return handle_err1(err)
  funcAsync2(res, (err2, res2) => {
    if (err2) return handle_err2(err2)
    funcAsync3(res2, (err3, res3) => {
      if (err3) return handle_err3(err3)
      return do_stuff(res3)
    })
  })
})
funcAsyncProm(a_val)
  .then(funcAsync2Prom, handle_err1)
  .then(funcAsync3Prom, handle_err2,)
  .then(handle_result, handle_err3)
#+END_SRC

#+reveal: split

Une promesse rejetée reste rejetée. 

Du coup, on peut simplifier le chaînage des =.then()=, et ne traiter l'erreur
qu'à la fin.

#+BEGIN_SRC javascript
funcAsyncProm(a_val)
  .then(funcAsync2Prom, undefined)
  .then(funcAsync3Prom, undefined)
  .then(handle_result, handle_err1_2_3)
// same as
funcAsyncProm(a_val)
  .then(funcAsync2Prom) // .then(x => funcAsync2Prom(x))
  .then(funcAsync3Prom) // .then(x => funcAsync3Prom(x))
  .then(handle_result)  // .then(x => handle_result(x))
  .then(undefined, handle_err1_2_3)
#+END_SRC

** Exemple 

[[file:examples/suspense.js][examples/suspense.js]]
#+BEGIN_SRC javascript :tangle examples/suspense.js
const readJSONwithSuspense = (fileName) =>
      readJSONprom(fileName).
      then(res => waitLogResolve(2, res)).
      then(null, err => waitLogReject(2, err))
#+END_SRC

#+BEGIN_SRC javascsript
.load examples/waitlog.js
.load examples/readjson.js
.load examples/suspense.js
let sp = readJSONwithSuspense('/tmp/test.json')
let sr = readJSONwithSuspense("I don't exist !")
#+END_SRC

** Imbriquer

Pour garder l'accès à un résultat intermédiaire:

#+BEGIN_SRC javascript
// async1 then async2
async1().then(res1 => async2(res1)).then(function(res2){
  // result res1 from async1 is not available
}

async1().then(function (res1) {
  return async2(res1).then(function (res2) {
    // do something with res1 AND res2
    return stuff(res1,res2)
  })
})

async1().then(res1 => {
  return async2(res1).
    then( res2 => stuff(res1, res2))
})

async1().then(res1 => 
  async2(res1).then(res2 => stuff(res1, res2))
)
#+END_SRC

#+REVEAL: split

#+BEGIN_SRC javascript
// INDENT !!!!
async1().then(res1 => async2(res1)).then(res2 => stuff(res1, res2))
//                        this )  ^                has moved here v
async1().then(res1 => async2(res1).then(res2 => stuff(res1, res2)))

async1().then(res1 =>
  async2(res1).then(res2 => stuff(res1,res2))
)
#+END_SRC

#+reveal: split
Les callbacks des promesses ne prennent en compte qu'un argument. 
On peut aussi utiliser un objet pour renvoyer plusieurs résultats.

#+begin_src javascript
async1()
  .then(res1 => [res1, async2(res1)])
  .then(a => {
    let res1 = a[0]
    let res2 = a[1] // ES6: let [res1, res2] = a
    stuff(res1, res2) // ES6 stuff(...a)  
})
#+end_src

Les notations avancées introduites par ES6 aident bien …

#+begin_src javascript
async1()
  .then(res1 => [res1, async2(res1)])
  .then(a => stuff(...a))
#+end_src

** Promesse et fonction synchrone
Un ~return~ d'un résultat qui n'est pas une promesses est automatiquement
encapsulé dans une promesse qui réussit tout le temps:
#+BEGIN_SRC javascript
// auto create promise from normal value
async().then(function(data) {
  return fSync(data)
})

// same as
async().then(function(data) {
  return new Promise(function(resolve, reject){
    resolve(fSync(data))
  })
#+END_SRC

** Notations simplifiée

#+BEGIN_SRC javascript
// always resolve with a value
Promise.resolve(val)
// same as
new Promise((resolve, reject) => {return resolve(val)})
new Promise((resolve, reject) => resolve(val))
// always reject
Promise.reject(reason)
// same as
new Promise((resolve, reject) => {return reject(reason)})
new Promise((resolve, reject) => reject(reason))
#+END_SRC

** Gestion des erreurs

- *try/catch* équivalent

#+BEGIN_SRC javascript
// version synchrone
try {
  doThis()
  doThat()
} catch (err) {
  console.error(err)
}

// version promesse 
doThisAsync()
  .then(doThatAsync)
  .then(undefined, console.error)

// si doThisAsync échoue, doThatAsync n'est pas appelée
// erreur traité par le prochain then avec un onRejected
#+END_SRC

#+REVEAL: split

- les promesses interceptent les erreurs de la VM
- les promesses interceptent les throw 
- =.catch(reject)=: notation simplifiée pour un =.then= ne gérant que le cas d'erreur

#+BEGIN_SRC javascript
doThisAsync()
  .then(doThatAsync)
  .then(undefined, console.error)
// same as
doThisAsync()
  .then(doThatAsync)
  .catch(console.error)

#+END_SRC


#+REVEAL: split

#+BEGIN_SRC javascript
doThisAsync()
  .then(function (data) {
    data.foo.baz = 'bar' 
    // throws a ReferenceError as foo is not defined
  })
  .then(undefined, console.error)
#+END_SRC

#+BEGIN_SRC javascript
doThisAsync()
  .then(function (data) {
    if (!data.baz) throw new Error('Expected baz to be there')
  })
  .catch(console.error) // catch(fn) is shorthand for .then(undefined, fn)
#+END_SRC

** Gérer des opérations en //

#+BEGIN_SRC javascript
let array_of_promises = [ fs_readfile('file1'), fs_readfile('file2')]
let p = Promise.all(array_of_promises)
p.then(function(array_of_results){ … }, onRejected)
#+END_SRC

- tout marche : on récupère un tableau des résultats
- la première erreur fait échouer la promesse

** Attendre le premier résultat d'opérations en //

#+BEGIN_SRC javascript
let array_of_promises = [ fs_readfile('file1'), fs_readfile('file2')]
let p = Promise.race(array_of_promises)
p.then(function(res){ … }, onRejected)
#+END_SRC

- la promesse renvoyée correspond à la première qui est résolue ou échoue.

** COMMENT Application: observer l'état interne d'une promesse

#+BEGIN_SRC javascript :tangle examples/ps.js 
function ps(p){
  p.status = 'pending'
  return p.then(
    r => {
      p.status = 'fullfilled'
      return r
    }, 
    r => { 
      p.status = 'rejected'
      return r
    }
  )
}
#+END_SRC

#+begin_src javascript
.load examples/waitlog.js
.load examples/readjson.js
.load examples/suspense.js
.load examples/ps.js
let sp = readJSONwithSuspense('/tmp/test.json')
ps(sp)
let sr = readJSONwithSuspense("I don't exist !")

./load 

#+end_src
* «Promissifer» du code node.js existant

Comme nombre de callback de node.js sont de la forme =(err, res) => {…}=, des
librairies permettent de «promissifier» une fonction node.js.

#+BEGIN_SRC shell
npm install promise
#+END_SRC 

#+BEGIN_SRC javascript
const Promise = require('promise') // overwrite default implementation
let readFile = Promise.denodeify(require('fs').readFile);
// now `readFile` will return a promise rather than
// expecting a callback
#+END_SRC

node.js > 8:  [[https://nodejs.org/dist/latest-v12.x/docs/api/util.html#util_util_promisify_original][util.promisify]] fait la même chose, mais en standard
* Écrire du code bilingue callback/promesse

Il suffit de tester la présence de l'argument représentant le callback:
- présent: on appelle le callback
- absent : on retourne une promesse

#+BEGIN_SRC javascript
function readFile(file, encoding, cb) {
  if (cb) return fs.readFile(file, encoding, cb)

  return new Promise(function (resolve, reject) {
    fs.readFile(file, encoding, function (err, data) {
      if (err) return reject(err)
      resolve(data)
    })
  })
}
#+END_SRC

Si besoin est, n'oubliez pas: ~typeof cb === "function"~
* Références

- [[https://developer.ibm.com/node/2016/08/24/promises-in-node-js-an-alternative-to-callbacks/][promises in node.js : an alternative to callback]]
- [[https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_promesses][MDN Utiliser les promesses]], [[https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise][MDN Promise]]
- [[https://www.promisejs.org/][promisejs.org]]

* Aller encore plus loin: async/await
En direct de ES8, mais déjà [[https://caniuse.com/#feat=async-functions][disponible]]
#+BEGIN_SRC javascript
function fetchTextByPromise() {
  return new Promise(resolve => { 
    setTimeout(() => { 
      resolve("es8");
    }, 2000);
  });
}

async function sayHello() { 
  const externalFetchedText = await fetchTextByPromise();
  console.log(`Hello, ${externalFetchedText}`); // Hello, es8
}

sayHello();
#+END_SRC

  [[https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/async_function][mdn async/await]] 

